qt6-scxml (6.8.2-3) unstable; urgency=medium

  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Sat, 01 Mar 2025 13:05:54 +0100

qt6-scxml (6.8.2-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Thu, 13 Feb 2025 12:09:20 +0100

qt6-scxml (6.8.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.8.2).
  * Bump Qt B-Ds to 6.8.2.
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Sat, 08 Feb 2025 17:55:41 +0100

qt6-scxml (6.7.2-3) unstable; urgency=medium

  [ Sandro Knauß ]
  * Use dh_qmldeps to detect QML dependencies.
  * Add qml6-module packages to -dev package for QML dependency detection.
  * Use dh-sequence-pkgkde-symbolshelper instead of --with.

 -- Patrick Franz <deltaone@debian.org>  Thu, 05 Dec 2024 00:07:42 +0100

qt6-scxml (6.7.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Sat, 05 Oct 2024 02:00:11 +0200

qt6-scxml (6.7.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.7.2).
  * Bump Qt B-Ds to 6.7.2.
  * Bump Standards-Version to 4.7.0 (no changes needed).
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Sat, 31 Aug 2024 21:15:33 +0200

qt6-scxml (6.6.2-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Thu, 06 Jun 2024 18:35:30 +0200

qt6-scxml (6.6.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.6.2).
  * Bump Qt B-Ds to 6.6.2.
  * Build-depend on pkgconf instead of pkg-config as the latter has been
    superseded by the former.

 -- Patrick Franz <deltaone@debian.org>  Fri, 16 Feb 2024 22:52:50 +0100

qt6-scxml (6.6.1-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Add missing Breaks+Replaces for libqt6scxmlqml6 and
    libqt6statemachineqml6 (Closes: #1061067).

 -- Patrick Franz <deltaone@debian.org>  Wed, 17 Jan 2024 15:36:56 +0100

qt6-scxml (6.6.1-1) experimental; urgency=medium

  [ Tianyu Chen ]
  * New upstream release.
  * Bump Qt B-Ds to 6.6.1.
  * Update list of installed files.
  * Update symbols from buildlogs.
  * Move libqt6{scxml,statemachine}qml6 to corresponding QML packages.
  * Fixed broken file references in qt6-scxml-doc-html doc-base configuration.
  * qt6-scxml-dev should not depend on QML modules.
  * Suppress lintian package-name-doesnt-match-sonames warnings for QML
    modules.

 -- Patrick Franz <deltaone@debian.org>  Tue, 16 Jan 2024 18:51:03 +0100

qt6-scxml (6.4.2-4) unstable; urgency=medium

  * Team upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 01 Aug 2023 13:33:41 -0300

qt6-scxml (6.4.2-3) experimental; urgency=medium

  * Team upload to unstable.

  [ Patrick Franz ]
  * Remove build-dependency on libqt6opengl6-dev as it has been merged
    into qt6-base-dev.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Add documentation and examples packages (Closes: #1042304).

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 28 Jul 2023 18:30:44 -0300

qt6-scxml (6.4.2-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Install the ECMAScript Data Model plugin (Closes: #1035066).

 -- Patrick Franz <deltaone@debian.org>  Mon, 01 May 2023 01:53:51 +0200

qt6-scxml (6.4.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Switch to the official 6.4.2 tarball, the tarball is the same.

 -- Patrick Franz <deltaone@debian.org>  Tue, 24 Jan 2023 12:46:29 +0100

qt6-scxml (6.4.2~rc1-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 30 Dec 2022 16:58:50 +0100

qt6-scxml (6.4.2~rc1-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.
  * Bump standards version to 4.6.2, no changes required.

 -- Patrick Franz <deltaone@debian.org>  Tue, 27 Dec 2022 22:27:25 +0100

qt6-scxml (6.4.1-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Pass QT_HOST_PATH to cmake in cross-builds, thx to Helmut Grohne
    (Closes: #1025064).

 -- Patrick Franz <deltaone@debian.org>  Thu, 08 Dec 2022 01:32:41 +0100

qt6-scxml (6.4.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.4.1).
  * Bump Qt B-Ds to 6.4.1.

 -- Patrick Franz <deltaone@debian.org>  Thu, 17 Nov 2022 20:15:42 +0100

qt6-scxml (6.4.0-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Enable link time optimization (Closes: #1015624).

 -- Patrick Franz <deltaone@debian.org>  Sat, 12 Nov 2022 15:41:01 +0100

qt6-scxml (6.4.0-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Bump minimum CMake version in order to get pkg-config's .pc files.

  [ Patrick Franz ]
  * Increase CMake verbosity level.
  * New upstream release (6.4.0).
  * Bump Qt B-Ds to 6.4.0.
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Sat, 15 Oct 2022 11:54:23 +0200

qt6-scxml (6.3.1-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Mon, 15 Aug 2022 19:23:40 +0200

qt6-scxml (6.3.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.1).
  * Bump Qt B-Ds to 6.3.1.
  * Bump Standards-Version to 4.6.1 (no changes needed).
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Sun, 17 Jul 2022 18:28:01 +0200

qt6-scxml (6.3.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.0).
  * Bump Qt B-Ds to 6.3.0.
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Wed, 15 Jun 2022 22:16:49 +0200

qt6-scxml (6.2.4-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 22 Apr 2022 20:07:13 +0200

qt6-scxml (6.2.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.2.4).
  * Bump Qt B-Ds to 6.2.4.

 -- Patrick Franz <deltaone@debian.org>  Thu, 31 Mar 2022 22:59:47 +0200

qt6-scxml (6.2.2-3) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 15 Feb 2022 22:46:48 +0100

qt6-scxml (6.2.2-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Source-only upload to experimental.

 -- Patrick Franz <deltaone@debian.org>  Thu, 30 Dec 2021 19:47:49 +0100

qt6-scxml (6.2.2-1) experimental; urgency=medium

  * Initial release (Closes: #999893)

 -- Patrick Franz <deltaone@debian.org>  Thu, 18 Nov 2021 08:26:37 +0100
